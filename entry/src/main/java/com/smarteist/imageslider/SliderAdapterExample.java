package com.smarteist.imageslider;

import com.smarteist.autoimageslider.*;
import com.smarteist.imageslider.Model.SliderItem;

import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapterExample extends PageSliderProvider {

    private Context context;
    private SliderPager sliderPager;
    public List<SliderItem> mSliderItems = new ArrayList<>();
    public List<ItemContainer> itemsContainer = new ArrayList<>();

    public SliderAdapterExample(Context context) {
        super();
        this.context = context;
    }

    public void renewItems(List<SliderItem> sliderItems) {
        mSliderItems = sliderItems;
        itemsContainer.clear();
        sliderPager.removeAllAnimations();
//        notifyDataChanged();

        for (int i = 0; i < mSliderItems.size(); i++) {
            ItemContainer itemContainer = new ItemContainer(context);
            itemContainer.setImageText(mSliderItems.get(i).getDescription(), mSliderItems.get(i).getImageUrl());
            itemsContainer.add(itemContainer);
        }
        sliderPager.renewAnimation(itemsContainer);
        sliderPager.setCurrentPage(0, true);
        notifyDataChanged();
    }

    public void deleteItem(int position) {
        sliderPager.setCurrentPage(0, true);
        mSliderItems.remove(position);
        itemsContainer.remove(position);
        sliderPager.removeAnimation(position);
        notifyDataChanged();
    }

    public void addItem(SliderItem sliderItem) {
        mSliderItems.add(sliderItem);
        ItemContainer itemContainer = new ItemContainer(context);
        itemContainer.setImageText(sliderItem.getDescription(), sliderItem.getImageUrl());
        itemsContainer.add(itemContainer);
        sliderPager.addAnimation(itemContainer, getCount() - 1);
        sliderPager.setCurrentPage(0, true);
        notifyDataChanged();
    }

    @Override
    public int getCount() {
        return itemsContainer.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Component component = itemsContainer.get(i);
        component.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(context).setText("This is item in position " + i).show();
            }
        });
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    public void setPageSlider(SliderPager sliderPager) {
        this.sliderPager = sliderPager;
    }
}
