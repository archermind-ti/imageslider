package com.smarteist.imageslider.slice;

import com.smarteist.autoimageslider.*;
import com.smarteist.autoimageslider.IndicatorView.animation.type.AnimationType;
import com.smarteist.imageslider.Model.SliderItem;
import com.smarteist.imageslider.ResourceTable;
import com.smarteist.imageslider.SliderAdapterExample;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    private SliderView sliderView;
    private SliderAdapterExample adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        sliderView = (SliderView) findComponentById(ResourceTable.Id_imageSlider);
        findComponentById(ResourceTable.Id_btn_add).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_remove).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_renew).setClickedListener(this);
        adapter = new SliderAdapterExample(getContext());
        adapter.setPageSlider(sliderView.getmPageSlider());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(AnimationType.COLOR); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.getIntColor("WHITE"));
        sliderView.setIndicatorUnselectedColor(Color.getIntColor("GRAY"));
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();
        renewItems();
    }

    public void renewItems() {
        List<SliderItem> sliderItemList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            SliderItem sliderItem = new SliderItem();
            sliderItem.setDescription("Slider Item " + i);
            if (i % 2 == 0) {
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png06));
            } else {
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png05));
            }
            sliderItemList.add(sliderItem);
        }
        adapter.renewItems(sliderItemList);
    }

    public void removeLastItem() {
        if (adapter.getCount() - 1 >= 0) {
            adapter.deleteItem(adapter.getCount() - 1);
        }
    }

    public void addNewItem() {
        SliderItem sliderItem = new SliderItem();
        sliderItem.setDescription("Slider Item Added Manually");
        int max = 6, min = 1;
        int ran = (int) (Math.random() * (max - min) + min);
        switch (ran) {
            case 1:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png01));
                break;
            case 2:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png02));
                break;
            case 3:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png03));
                break;
            case 4:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png04));
                break;
            case 5:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png05));
                break;
            case 6:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png06));
                break;
            default:
                sliderItem.setImageUrl(Integer.toString(ResourceTable.Media_png06));
                break;
        }
        adapter.addItem(sliderItem);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_add:
                addNewItem();
                break;
            case ResourceTable.Id_btn_remove:
                removeLastItem();
                break;
            case ResourceTable.Id_btn_renew:
                renewItems();
                break;
            default:
                break;
        }
    }
}
