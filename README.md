# Image slider

This is an amazing image slider.
 
You can easily load images with your custom layout, and there are many kinds of amazing animations you can choose.

## Integration

#### Use har lib
```
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

#### Gradle

`implementation 'com.gitee.archermind-ti:autoimageslider:1.0.0-beta'`

## New Feautures
* Ability to disable default indicator.

## New Changes
* Swiping debounce implemented.

## Demo

<img src="/screenShoot/image.gif" width="300" height="569"/>

## Integration guide

First put the slider view in your layout xml :

```xml
    <com.smarteist.autoimageslider.SliderView
        ohos:id="$+id:imageSlider"
        ohos:height="300vp"
        ohos:width="match_parent"
        />
```

## Use more Slider switching effects

You can change the value of AnimationType to change the effect.
sliderView.setIndicatorAnimation(AnimationType.COLOR);

For example:

AnimationType.SCALE_DOWN
AnimationType.DROP
AnimationType.FILL
AnimationType.SWAP
AnimationType.WORM
AnimationType.THIN_WORM

## Use more Picture switching effects

You can change the value of SliderAnimations to change the effect.
sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);

For example:

SliderAnimations.FIDGETSPINTRANSFORMATION
SliderAnimations.ANTICLOCKSPINTRANSFORMATION
SliderAnimations.CLOCK_SPINTRANSFORMATION
SliderAnimations.CUBEINDEPTHTRANSFORMATION
SliderAnimations.ZOOMOUTTRANSFORMATION
SliderAnimations.VERTICALFLIPTRANSFORMATION

## Next step

The new version requires an slider adapter plus your custom layout for slider items, Although its very similar to RecyclerView & RecyclerAdapter, and it's familiar and easy to implement this adapter... here is an example for adapter implementation :

```java
public class SliderAdapterExample extends PageSliderProvider {

    private Context context;
    private SliderPager sliderPager;
    public List<SliderItem> mSliderItems = new ArrayList<>();
    public List<ItemContainer> itemsContainer = new ArrayList<>();

    public SliderAdapterExample(Context context) {
        super();
        this.context = context;
    }

    public void renewItems(List<SliderItem> sliderItems) {
        mSliderItems = sliderItems;
        itemsContainer.clear();
        sliderPager.removeAllAnimations();

        for (int i = 0; i < mSliderItems.size(); i++) {
            ItemContainer itemContainer = new ItemContainer(context);
            itemContainer.setImageText(mSliderItems.get(i).getDescription(), mSliderItems.get(i).getImageUrl());
            itemsContainer.add(itemContainer);
        }
        sliderPager.renewAnimation(itemsContainer);
        sliderPager.setCurrentPage(0, true);
        notifyDataChanged();
    }

    public void deleteItem(int position) {
        sliderPager.setCurrentPage(0, true);
        mSliderItems.remove(position);
        itemsContainer.remove(position);
        sliderPager.removeAnimation(position);
        notifyDataChanged();
    }

    public void addItem(SliderItem sliderItem) {
        mSliderItems.add(sliderItem);
        ItemContainer itemContainer = new ItemContainer(context);
        itemContainer.setImageText(sliderItem.getDescription(), sliderItem.getImageUrl());
        itemsContainer.add(itemContainer);
        sliderPager.addAnimation(itemContainer, getCount() - 1);
        sliderPager.setCurrentPage(0, true);
        notifyDataChanged();
    }

    @Override
    public int getCount() {
        return itemsContainer.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Component component = itemsContainer.get(i);
        component.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new ToastDialog(context).setText("This is item in position " + i).show();
            }
        });
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }

    public void setPageSlider(SliderPager sliderPager) {
        this.sliderPager = sliderPager;
    }
}
```

## Set the adapter to the Sliderview

After the instantiating of the sliderView (inside the activity or fragment with findViewById|BindView...), set the adapter to the slider.

```java
    adapter = new SliderAdapterExample(getContext());
    adapter.setPageSlider(sliderView.getmPageSlider());
    sliderView = (SliderView) findComponentById(ResourceTable.Id_imageSlider);
    sliderView.setSliderAdapter(adapter);
```

## Elaborate more?

Here is a more realistic and more complete example :

```java
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        sliderView = (SliderView) findComponentById(ResourceTable.Id_imageSlider);
        findComponentById(ResourceTable.Id_btn_add).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_remove).setClickedListener(this);
        findComponentById(ResourceTable.Id_btn_renew).setClickedListener(this);
        adapter = new SliderAdapterExample(getContext());
        adapter.setPageSlider(sliderView.getmPageSlider());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(AnimationType.COLOR); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.getIntColor("WHITE"));
        sliderView.setIndicatorUnselectedColor(Color.getIntColor("GRAY"));
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();
        renewItems();
    }
```

## Licence

Copyright [2019]

   Licensed under the Apache License, Version 2.0;
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at


   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
