package com.smarteist.autoimageslider;

import com.smarteist.autoimageslider.IndicatorView.PageIndicatorView;
import com.smarteist.autoimageslider.IndicatorView.animation.type.AnimationType;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.multimodalinput.event.TouchEvent;

public class SliderView extends StackLayout implements Runnable, Component.TouchEventListener, PageSlider.PageChangedListener, Component.EstimateSizeListener {

    public static final int AUTO_CYCLE_DIRECTION_RIGHT = 0;
    public static final int AUTO_CYCLE_DIRECTION_LEFT = 1;
    public static final int AUTO_CYCLE_DIRECTION_BACK_AND_FORTH = 2;
    private int mScrollTimeInMillis;
    private final EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());
    private PageIndicatorView mPagerIndicator;
    private SliderPager mPageSlider;
    private boolean mIsAutoCycle;
    private int mAutoCycleDirection;
    private boolean mFlagBackAndForth = true;

    public SliderView(Context context) {
        this(context, null);
    }

    public SliderView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SliderView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setupSlideView(context);
        initIndicator();
    }

    private void initIndicator() {
        if (mPagerIndicator == null) {
            mPagerIndicator = new PageIndicatorView(getContext());
            LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
            params.alignment = LayoutAlignment.HORIZONTAL_CENTER | LayoutAlignment.BOTTOM;
            params.setMargins(20, 20, 20, 20);
            mPagerIndicator.setLayoutConfig(params);
            addComponent(mPagerIndicator, 1, params);
        }
        mPagerIndicator.setPageSlider(mPageSlider);
        mPagerIndicator.setDynamicCount(true);
    }

    private void setupSlideView(Context context) {
        mPageSlider = new SliderPager(context);
        addComponent(mPageSlider, 0);
        mPageSlider.setTouchEventListener(this);
        mPageSlider.getListener(this);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        if (isAutoCycle()) {
            switch (event.getAction()) {
                case TouchEvent.PRIMARY_POINT_DOWN:
                case TouchEvent.POINT_MOVE:
                    stopAutoCycle();
                    break;
                case TouchEvent.PRIMARY_POINT_UP:
                    mHandler.postTask(new Runnable() {
                        @Override
                        public void run() {
                            startAutoCycle();
                        }
                    }, 2000);
                    break;
            }
        }
        return true;
    }

    public void startAutoCycle() {
        //clean previous callbacks
        mHandler.removeTask(this);

        //Run the loop for the first time
        mHandler.postTask(this, mScrollTimeInMillis);
    }

    private void stopAutoCycle() {
        mHandler.removeTask(this);
    }

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSlideStateChanged(int i) {
    }

    @Override
    public void onPageChosen(int i) {
        mPagerIndicator.setSelection(i);
    }

    public void setSliderAdapter(PageSliderProvider adapter) {
        mPageSlider.setProvider(adapter);
    }

    public SliderPager getmPageSlider() {
        return mPageSlider;
    }

    public void setScrollTimeInSec(int time) {
        mScrollTimeInMillis = time * 1000;
    }

    public int getScrollTimeInSec() {
        return mScrollTimeInMillis / 1000;
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimatedConfig);
        int height = Component.EstimateSpec.getSize(heightEstimatedConfig);
        setEstimatedSize(width, height);
        ComponentContainer.LayoutConfig sliderParams = mPageSlider.getLayoutConfig();
        sliderParams.width = width;
        sliderParams.height = height;
        mPageSlider.setLayoutConfig(sliderParams);
        return false;
    }

    public void setSliderTransformAnimation(SliderAnimations sliderAnimations) {
        mPageSlider.setSliderTransformAnimation(sliderAnimations);
    }

    public boolean isAutoCycle() {
        return mIsAutoCycle;
    }

    public void setAutoCycle(boolean autoCycle) {
        this.mIsAutoCycle = autoCycle;
    }

    public void setIndicatorUnselectedColor(int color) {
        mPagerIndicator.setUnselectedColor(color);
    }

    public void setIndicatorSelectedColor(int color) {
        mPagerIndicator.setSelectedColor(color);
    }

    public void setAutoCycleDirection(int direction) {
        mAutoCycleDirection = direction;
    }

    public void setIndicatorAnimation(AnimationType animation) {
        mPagerIndicator.setAnimationType(animation);
    }

    @Override
    public void run() {
        try {
            slideToNextPosition();
        } finally {
            if (mIsAutoCycle) {
                // continue the loop
                mHandler.postTask(this, mScrollTimeInMillis);
            }
        }
    }

    private void slideToNextPosition() {
        int currentPosition = mPageSlider.getCurrentPage();
        int adapterItemsCount = getAdapterItemsCount();
        if (adapterItemsCount > 1) {
            if (mAutoCycleDirection == AUTO_CYCLE_DIRECTION_BACK_AND_FORTH) {
                if (currentPosition == (adapterItemsCount - 1)) {
                    mFlagBackAndForth = false;
                }
                if (currentPosition == 0) {
                    mFlagBackAndForth = true;
                }
                if (mFlagBackAndForth) {
                    mPageSlider.setCurrentPage(currentPosition + 1, true);
                } else {
                    mPageSlider.setCurrentPage(currentPosition - 1, true);
                }
            }
            if (mAutoCycleDirection == AUTO_CYCLE_DIRECTION_LEFT) {
                if (currentPosition == 0) {
                    mPageSlider.setCurrentPage(adapterItemsCount - 1,true);
                }else {
                    mPageSlider.setCurrentPage(currentPosition - 1, true);
                }
            }
            if (mAutoCycleDirection == AUTO_CYCLE_DIRECTION_RIGHT) {
                if (currentPosition == (adapterItemsCount - 1)) {
                    mPageSlider.setCurrentPage(0,true);
                }else {
                    mPageSlider.setCurrentPage(currentPosition + 1, true);
                }
            }
        }
    }

    private int getAdapterItemsCount() {
        try {
            return mPageSlider.getProvider().getCount();
        } catch (NullPointerException e) {
            return 0;
        }
    }
}
