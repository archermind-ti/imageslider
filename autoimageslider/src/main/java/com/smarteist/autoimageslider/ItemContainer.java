/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smarteist.autoimageslider;

import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

public class ItemContainer extends StackLayout implements Component.EstimateSizeListener {

    private Text text;
    private Image image;

    public ItemContainer(Context context) {
        this(context, null);
    }

    public ItemContainer(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public ItemContainer(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
        setEstimateSizeListener(this);
    }

    private void init(Context context) {
        LayoutConfig layoutConfigText = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        layoutConfigText.alignment = LayoutAlignment.BOTTOM;
        text = new Text(context);
        text.setTextColor(new Color(Color.getIntColor("WHITE")));
        text.setTextSize(50);
        text.setPadding(20,30,20,90);
        ShapeElement element = new ShapeElement(getContext(), ResourceTable.Graphic_text_background);
        text.setBackground(element);
        image = new Image(context);
        image.setScaleMode(Image.ScaleMode.STRETCH);
        addComponent(image, 0);
        addComponent(text, 1, layoutConfigText);
    }

    public void setImageText(String text1, String url) {
        image.setPixelMap(Integer.parseInt(url));
        text.setText(text1);
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        int width = Component.EstimateSpec.getSize(widthEstimatedConfig);
        int height = Component.EstimateSpec.getSize(heightEstimatedConfig);
        setEstimatedSize(width, height);
        ComponentContainer.LayoutConfig layoutConfig = image.getLayoutConfig();
        layoutConfig.width = width;
        layoutConfig.height = height;
        image.setLayoutConfig(layoutConfig);
        return false;
    }
}
