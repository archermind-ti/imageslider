package com.smarteist.autoimageslider.Transformations;

import com.smarteist.autoimageslider.SliderPageAnimation;
import ohos.agp.components.Component;

public class ZoomOutTransformation extends SliderPageAnimation {
    /**
     * 第page页需要移动的x值
     */
    public int xPosition;
    private static final float MIN_SCALE = 0.65f;
    private static final float MIN_ALPHA = 0.3f;

    /**
     * 第page页需要移动的y值
     */
    public int yPosition;

    private float xStartPosition;
    private float yStartPosition;

    /**
     * @param forPage page to apply animation
     * @param dx      x moving, in dp
     * @param dy      y moving, in dp
     */
    public ZoomOutTransformation(int forPage, int dx, int dy) {
        this.page = forPage;
        this.xPosition = dx;
        this.yPosition = dy;
        this.xStartPosition = 0;
        this.yStartPosition = 0;
    }

    /**
     * 设置具体控件最开始状态的位置偏移量
     *
     * @param startX x偏移量
     * @param startY y偏移量
     */
    public void setStart(Integer startX, Integer startY) {
        if (startX != null) {
            this.xStartPosition = startX;
        }
        if (startY != null) {
            this.yStartPosition = startY;
        }
    }

    @Override
    public void applyTransformation(Component page, float position) {
        if (position <-1){  // [-Infinity,-1)
            // This page is way off-screen to the left.
            page.setAlpha(0);

        }
        else if (position <=1){ // [-1,1]

            page.setScaleX(Math.max(MIN_SCALE,1-Math.abs(position)));
            page.setScaleY(Math.max(MIN_SCALE,1-Math.abs(position)));
            page.setAlpha(Math.max(MIN_ALPHA,1-Math.abs(position)));

        }
        else {  // (1,+Infinity]
            // This page is way off-screen to the right.
            page.setAlpha(0);

        }
    }
}
