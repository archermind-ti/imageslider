package com.smarteist.autoimageslider.Transformations;

import com.smarteist.autoimageslider.SliderPageAnimation;
import ohos.agp.components.Component;

public class FadeTransformation extends SliderPageAnimation {
    /**
     * 第page页需要移动的x值
     */
    public int xPosition;

    /**
     * 第page页需要移动的y值
     */
    public int yPosition;

    private float xStartPosition;
    private float yStartPosition;

    /**
     * @param forPage page to apply animation
     * @param dx      x moving, in dp
     * @param dy      y moving, in dp
     */
    public FadeTransformation(int forPage, int dx, int dy) {
        this.page = forPage;
        this.xPosition = dx;
        this.yPosition = dy;
        this.xStartPosition = 0;
        this.yStartPosition = 0;
    }

    /**
     * 设置具体控件最开始状态的位置偏移量
     *
     * @param startX x偏移量
     * @param startY y偏移量
     */
    public void setStart(Integer startX, Integer startY) {
        if (startX != null) {
            this.xStartPosition = startX;
        }
        if (startY != null) {
            this.yStartPosition = startY;
        }
    }

    @Override
    public void applyTransformation(Component view, float position) {
        view.setTranslationX(-position*view.getWidth());

        // Page is not an immediate sibling, just make transparent
        if(position < -1 || position > 1) {
            view.setAlpha(0);
        }
        // Page is sibling to left or right
        else if (position <= 0 || position <= 1) {

            // Calculate alpha.  Position is decimal in [-1,0] or [0,1]
            float alpha = (position <= 0) ? position + 1 : 1 - position;
            view.setAlpha(alpha);

        }
        // Page is active, make fully visible
        else if (position == 0) {
            view.setAlpha(1);
        }
    }
}
