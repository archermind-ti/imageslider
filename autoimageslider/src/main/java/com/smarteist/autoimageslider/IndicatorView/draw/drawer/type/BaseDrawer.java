package com.smarteist.autoimageslider.IndicatorView.draw.drawer.type;

import com.smarteist.autoimageslider.IndicatorView.draw.data.Indicator;
import ohos.agp.render.Paint;

class BaseDrawer {
    Paint paint;
    Indicator indicator;

    BaseDrawer(Paint paint, Indicator indicator) {
        this.paint = paint;
        this.indicator = indicator;
    }
}
