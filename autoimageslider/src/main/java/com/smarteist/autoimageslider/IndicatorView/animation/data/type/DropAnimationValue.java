package com.smarteist.autoimageslider.IndicatorView.animation.data.type;

import com.smarteist.autoimageslider.IndicatorView.animation.data.Value;
import com.smarteist.autoimageslider.IndicatorView.animation.type.DropAnimation;

public class DropAnimationValue implements Value {
    private int width;
    private int height;
    private int radius;
    private DropAnimation.AnimationType type;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public DropAnimation.AnimationType getType() {
        return type;
    }

    public void setType(DropAnimation.AnimationType type) {
        this.type = type;
    }
}
