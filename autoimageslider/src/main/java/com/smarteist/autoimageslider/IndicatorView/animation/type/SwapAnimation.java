package com.smarteist.autoimageslider.IndicatorView.animation.type;

import com.smarteist.autoimageslider.IndicatorView.animation.controller.ValueController;
import com.smarteist.autoimageslider.IndicatorView.animation.data.type.SwapAnimationValue;
import com.smarteist.autoimageslider.IndicatorView.utils.AnimatorUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

public class SwapAnimation extends BaseAnimation<ValueAnimator> {
    private static final String ANIMATION_COORDINATE = "ANIMATION_COORDINATE";
    private static final String ANIMATION_COORDINATE_REVERSE = "ANIMATION_COORDINATE_REVERSE";
    private static final int COORDINATE_NONE = -1;

    private int coordinateStart = COORDINATE_NONE;
    private int coordinateEnd = COORDINATE_NONE;

    private SwapAnimationValue value;

    public SwapAnimation(ValueController.UpdateListener listener) {
        super(listener);
        value = new SwapAnimationValue();
    }

    @Override
    public ValueAnimator createAnimator() {
        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(BaseAnimation.DEFAULT_ANIMATION_TIME);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setValueUpdateListener(
                new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        animator.onUpdate(animatorValue, v);
                        onAnimateUpdated(animatorValue);
                    }
                });

        return animator;
    }

    @Override
    public SwapAnimation progress(float progress) {
        if (animator != null) {
            animator.onUpdate(animator, AnimatorUtils.getInterpolation(progress));
            onAnimateUpdated(animator);
        }

        return this;
    }

    public SwapAnimation with(int coordinateStart, int coordinateEnd) {
        if (animator != null && hasChanges(coordinateStart, coordinateEnd)) {
            this.coordinateStart = coordinateStart;
            this.coordinateEnd = coordinateEnd;

            PropertyValuesHolder holder =
                    createColorPropertyHolder(ANIMATION_COORDINATE, coordinateStart, coordinateEnd);
            PropertyValuesHolder holderReverse =
                    createColorPropertyHolder(ANIMATION_COORDINATE_REVERSE, coordinateEnd, coordinateStart);
            animator.setInnerListener(
                    new InnerValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float f) {
                            coordinate = AnimatorUtils.getIntValue(holder.getStartValue(), holder.getEndValue(), f);
                            coordinateReverse =
                                    AnimatorUtils.getIntValue(
                                            holderReverse.getStartValue(), holderReverse.getEndValue(), f);
                        }
                    });
            //            animator.setValues(holder, holderReverse);
        }

        return this;
    }

    private PropertyValuesHolder createColorPropertyHolder(String propertyName, int startValue, int endValue) {
        PropertyValuesHolder holder = PropertyValuesHolder.ofInt(startValue, endValue);

        return holder;
    }

    int coordinate;
    int coordinateReverse;

    private void onAnimateUpdated(AnimatorValue animation) {
        value.setCoordinate(coordinate);
        value.setCoordinateReverse(coordinateReverse);

        if (listener != null) {
            listener.onValueUpdated(value);
        }
    }

    @SuppressWarnings("RedundantIfStatement")
    private boolean hasChanges(int coordinateStart, int coordinateEnd) {
        if (this.coordinateStart != coordinateStart) {
            return true;
        }

        if (this.coordinateEnd != coordinateEnd) {
            return true;
        }

        return false;
    }
}
