package com.smarteist.autoimageslider.IndicatorView.utils;

import ohos.app.Context;

public class DensityUtils {
    public static int vpToPx(int dp, Context mContext) {
        return (int) (mContext.getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
