/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.smarteist.autoimageslider;

import com.smarteist.autoimageslider.Transformations.*;
import ohos.agp.components.Component;

import java.util.ArrayList;
import java.util.HashMap;

public class SliderViewAnimation {

    private Component view;
    private HashMap<Integer, ArrayList<SliderPageAnimation>> pageAnimationMap;
    private int startX;
    private int startY;

    public SliderViewAnimation(Component inView) {
        this.view = inView;
        this.pageAnimationMap = new HashMap<Integer, ArrayList<SliderPageAnimation>>();
    }

    public void startToPosition(Integer xPosition, Integer yPosition) {
        if (xPosition != null) {
            this.view.setTranslationX(xPosition);
            startX = xPosition;
        }
        if (yPosition != null) {
            this.view.setTranslationY(yPosition);
            startY = yPosition;
        }
        this.view.postLayout();
    }

    public void setAnimation(SliderPageAnimation sliderPageAnimation){
        ArrayList<SliderPageAnimation> animationList = pageAnimationMap.get(sliderPageAnimation.page);
        if (animationList == null) {
            animationList = new ArrayList<SliderPageAnimation>();
        }
        animationList.add(sliderPageAnimation);
        pageAnimationMap.put(sliderPageAnimation.page, animationList);
    }

    public void addPageAnimation(SliderAnimations animation, int forPage, int dx, int dy) {
        SliderPageAnimation sliderPageAnimation;
        switch (animation) {
            case ANTICLOCKSPINTRANSFORMATION:
                sliderPageAnimation = new AntiClockSpinTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CLOCK_SPINTRANSFORMATION:
                sliderPageAnimation = new Clock_SpinTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEINDEPTHTRANSFORMATION:
                sliderPageAnimation = new CubeInDepthTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEINROTATIONTRANSFORMATION:
                sliderPageAnimation = new CubeInRotationTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEINSCALINGTRANSFORMATION:
                sliderPageAnimation = new CubeInScalingTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEOUTDEPTHTRANSFORMATION:
                sliderPageAnimation = new CubeOutDepthTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEOUTROTATIONTRANSFORMATION:
                sliderPageAnimation = new CubeOutRotationTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case CUBEOUTSCALINGTRANSFORMATION:
                sliderPageAnimation = new CubeOutScalingTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case DEPTHTRANSFORMATION:
                sliderPageAnimation = new DepthTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case FADETRANSFORMATION:
                sliderPageAnimation = new FadeTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case FANTRANSFORMATION:
                sliderPageAnimation = new FanTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case FIDGETSPINTRANSFORMATION:
                sliderPageAnimation = new FidgetSpinTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case GATETRANSFORMATION:
                sliderPageAnimation = new GateTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case HINGETRANSFORMATION:
                sliderPageAnimation = new HingeTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case HORIZONTALFLIPTRANSFORMATION:
                sliderPageAnimation = new HorizontalFlipTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case POPTRANSFORMATION:
                sliderPageAnimation = new PopTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case SPINNERTRANSFORMATION:
                sliderPageAnimation = new SpinnerTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case TOSSTRANSFORMATION:
                sliderPageAnimation = new TossTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case VERTICALFLIPTRANSFORMATION:
                sliderPageAnimation = new VerticalFlipTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case VERTICALSHUTTRANSFORMATION:
                sliderPageAnimation = new VerticalShutTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case ZOOMOUTTRANSFORMATION:
                sliderPageAnimation = new ZoomOutTransformation(forPage, dx, dy);
                setAnimation(sliderPageAnimation);
                break;
            case SIMPLETRANSFORMATION:
                sliderPageAnimation = new SimpleTransformation();
                setAnimation(sliderPageAnimation);
                break;
            default:
                sliderPageAnimation = new SimpleTransformation();
                setAnimation(sliderPageAnimation);
                break;
        }
    }

    protected void applyAnimation(int page, float positionOffset) {
        ArrayList<SliderPageAnimation> animationList = pageAnimationMap.get(page);
        if (animationList == null) {
            return;
        }
        for (SliderPageAnimation animation : animationList) {
            animation.applyTransformation(this.view, positionOffset);
        }
    }

}
