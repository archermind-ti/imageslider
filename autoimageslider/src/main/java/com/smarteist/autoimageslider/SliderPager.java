package com.smarteist.autoimageslider;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class SliderPager extends PageSlider {

    private ArrayList<SliderViewAnimation> mViewAnimation;
    private PageChangedListener pageChangedListener;
    private int positionOffset1;
    private int positionOffset2;
    private SliderAnimations sliderAnimations;

    public SliderPager(Context context) {
        this(context, null);
    }

    public SliderPager(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public SliderPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.mViewAnimation = new ArrayList<>();
    }

    public void getListener(PageChangedListener pageChangedListener) {
        this.pageChangedListener = pageChangedListener;
        setListener();
    }

    private void setListener() {
        addPageChangedListener(new PageChangedListener() {
            @Override
            public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffset1 == 0) {
                    positionOffset1 = positionOffsetPixels;
                } else if (positionOffset1 > 0) {
                    if (positionOffsetPixels < 0 || positionOffsetPixels < positionOffset1) {
                        positionOffset2 = positionOffsetPixels;
                    } else {
                        positionOffset1 = positionOffsetPixels;
                    }
                } else {
                    if (positionOffsetPixels > 0 || positionOffsetPixels > positionOffset1) {
                        positionOffset2 = positionOffsetPixels;
                    } else {
                        positionOffset1 = positionOffsetPixels;
                    }
                }
                float myPositionOffset = (float) positionOffsetPixels / (float) getWidth();
                if (positionOffset1 < 0) {
                    if (positionOffset2 > 0) {
                        myPositionOffset = myPositionOffset - 1;
                    }
                } else if (positionOffset1 > 0) {
                    if (positionOffset2 < 0) {
                        myPositionOffset = 1 + myPositionOffset;
                    }
                }
                if (positionOffset == 1 || positionOffset < 0.05) {
                    positionOffset1 = 0;
                    positionOffset2 = 0;
                    position = getCurrentPage();
                    myPositionOffset = 0;
                }
                if (myPositionOffset < 0) {
                    position--;
                    myPositionOffset = 1 + myPositionOffset;
                }

                for (int i = 0; i < mViewAnimation.size(); i++) {
                    mViewAnimation.get(i).applyAnimation(position, myPositionOffset);
                }
            }

            @Override
            public void onPageSlideStateChanged(int state) {
                if (pageChangedListener != null) {
                    pageChangedListener.onPageSlideStateChanged(state);
                }
                if (state == SLIDING_STATE_IDLE) {
                    for (int j = getChildCount(); j >= 0; j--) {
                        for (int i = 0; i < mViewAnimation.size(); i++) {
                            mViewAnimation.get(i).applyAnimation(j, 0);
                        }
                    }
                    for (int j = 0; j < getCurrentPage(); j++) {
                        for (int i = 0; i < mViewAnimation.size(); i++) {
                            mViewAnimation.get(i).applyAnimation(j, 1);
                        }
                    }
                }
            }

            @Override
            public void onPageChosen(int position) {
                if (pageChangedListener != null) {
                    pageChangedListener.onPageChosen(position);
                }
            }
        });
    }

    public void renewAnimation(List<ItemContainer> itemsContainer) {
        for (int i = 0; i < itemsContainer.size(); i++) {
            SliderViewAnimation nameTagAnimation = new SliderViewAnimation(itemsContainer.get(i));
            nameTagAnimation.addPageAnimation(sliderAnimations, i, 0, 0);
            mViewAnimation.add(nameTagAnimation);
        }
    }

    public void removeAnimation(int position) {
        mViewAnimation.remove(position);
    }

    public void removeAllAnimations() {
        mViewAnimation.clear();
    }

    public void addAnimation(ItemContainer itemContainer, int i) {
        SliderViewAnimation nameTagAnimation = new SliderViewAnimation(itemContainer);
        nameTagAnimation.addPageAnimation(sliderAnimations, i, 0, 0);
        mViewAnimation.add(nameTagAnimation);
    }

    public void setSliderTransformAnimation(SliderAnimations sliderAnimations) {
        this.sliderAnimations = sliderAnimations;
    }
}